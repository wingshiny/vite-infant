/*
 * @Author: YinXuan
 * @Date: 2022-01-30 21:48:57
 * @LastEditTime: 2023-02-16 10:22:11
 * @Description:
 */
module.exports = {
	root: true,
	env: {
		node: true,
	},
	extends: ['plugin:vue/recommended', 'eslint:recommended', 'plugin:import/errors', 'plugin:import/warnings', '@vue/prettier'],
	plugins: ['vue'],
	parserOptions: {
		parser: 'babel-eslint',
	},
	rules: {
		indent: [1, 2],
		eqeqeq: [1, 'always'],
		'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
		'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
		'import/no-unresolved': 'off',
		'vue/valid-v-model': 'off',
		'vue/no-deprecated-slot-attribute': 'off',
		'vue/no-v-html': 'off',
		'vue/no-dupe-keys': 'off',
		'vue/require-default-prop': 'off',
		'vue/require-prop-types': 'off',
		'vue/no-template-key': 'off',
		'prettier/prettier': [
			'off',
			{
				semi: false,
				singleQuote: true,
				endOfLine: 'auto',
			},
		],
	},
	overrides: [
		{
			files: ['*.vue'],
			rules: {},
		},
	],
	settings: {
		'import/resolver': {
			//配置/src目录下的索引
			alias: {
				map: [['/@/', 'src']],
			},
		},
	},
};
