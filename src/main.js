import { createApp } from 'vue'
import App from './App.vue'
// router
import router from "./router";
// store
import store from "./store";
import Antd from 'ant-design-vue'
import "ant-design-vue/dist/antd.less";
import * as Icons from "@ant-design/icons-vue"
import hljs from 'highlight.js' //导入代码高亮文件
import 'highlight.js/styles/xt256.css'  //导入代码高亮样式
import PreCode from "./components/PreCode.vue";
import TabsBar from "./components/TabsBar.vue";
import animated from "animate.css"
import * as echarts from 'echarts'
import 'echarts-wordcloud/dist/echarts-wordcloud'
import 'echarts-liquidfill/dist/echarts-liquidfill'
import ElementPlus from "element-plus";
import 'element-plus/dist/index.css'
import * as ELIcons from '@element-plus/icons-vue'

const app = createApp(App);
app.use(router);
app.use(store);
app.use(Antd);
app.use(animated);
app.use(ElementPlus);
// 全局挂载 echarts
app.config.globalProperties.$echarts = echarts;
//Vue3 使用 highlight.js
app.directive('highlight', {
	mounted(el) {
		let blocks = el.querySelectorAll('pre code');
		for (let i = 0; i < blocks.length; i++) {
			hljs.highlightElement(blocks[i]);
		}
	}
});
for (const i in Icons) {
	app.component(i, Icons[i]);
}
for (let iconName in ELIcons) {
	app.component(iconName, ELIcons[iconName])
}
app.component("PreCode", PreCode);
app.component("TabsBar", TabsBar);
app.mount("#app");
