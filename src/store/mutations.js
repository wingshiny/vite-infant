const mutations = {
	addCount(state) {
		state.counter++;
		console.log("state.counter: " + state.counter)
	},
	changeLoading(state) {
		state.loading = !state.loading;
	},
	setPhone(state, isMobile) {
		state.isMobile = isMobile;
	}
};

export default mutations