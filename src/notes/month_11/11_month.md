##vue-cli3打包之后的文件为何不能本地打开
##### vue-cli提供了一个入口让你能配置修改webpack.
* 在根目录下新建一个vue.config.js文件, 然后在其中修改publicPath这个选项:  
vue.config.js:  
``
    module.exports = {
     publicPath: './'
    }
``
***
##### 显示图片
![](https://tva3.sinaimg.cn/thumbnail/006K89bWgy1gwfuw1gw71j30u00u0myi.jpg)
***

*斜体文本*
_斜体文本_
**粗体文本**
__粗体文本__
***粗斜体文本***
___粗斜体文本___
**这是加粗的文字** 
 
*cat's meow*  
**bold text**  
This text is ***really important***.


***

<u>带下划线文本</u>

***
> 区块引用  
> 菜鸟教程  
> 学的不仅是技术更是梦想


* 第一项
    > 菜鸟教程  
    > 学的不仅是技术更是梦想
* 第二项

***

这是一个链接 [菜鸟教程](https://www.runoob.com)

***


