##绝对定位元素设置水平居中
````
方法一：
.element {
    position: absolute; 
    left: 50%; 
    top: 50%;
    transform: translate(-50%, -50%); /* 50%为自身尺寸的一半 */
    -webkit-transform: translate(-50%, -50%);
}
方法二：
.element {
    width: 600px; height: 400px;
    position: absolute;
    left: 0; 
    top: 0; 
    right: 0; 
    bottom: 0;
    margin: auto;  /* 有了这个就自动居中了 */
}
````
