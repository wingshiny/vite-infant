import {gameList} from "./gameList";

// 获取列表
export const getNbaGameUrl = (name = 'Lakers') => {
	return gameList.get(name);
};