export const songList = new Map([
	['Jay',
		[
			{
				title: "The Crew",
				name: "天地一斗",
				singerImg: "Jay",
				chooseLink: "Jay",
			},
		]
	],
	['Vae',
		[
			{
				title: "呼吸之野",
				name: "科幻",
				singerImg: "Vae",
				chooseLink: "Vae",
			},
		]
	],
	['Eason',
		[
			// {
			// 	title: "怎么样",
			// 	name: "对不起 谢谢",
			// 	singerImg: 'Eason',
			// 	chooseLink: "Eason",
			// },
			{
				title: "认了吧",
				name: "白色球鞋",
				singerImg: 'Eason',
				chooseLink: "Eason_QiuXie",
			},
			{
				title: "不想放手",
				name: "不要说话",
				singerImg: 'Eason',
				chooseLink: "Eason_BuYaoShuoHua",
			},
		]
	],
	['Mayday',
		[
			{
				title: "自传",
				name: "成名在望",
				singerImg: "Mayday",
				chooseLink: "Mayday_ChengMingZaiWang",
			},
		]
	],
]);
