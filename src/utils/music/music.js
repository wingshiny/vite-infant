
import {musicList} from "./musicList";
import {songList} from "./songList";

export const getMusicUrl = name => {
	return musicList.get(name);
};

export const getSongList = name => {
	return songList.get(name);
};

export const getSingerImg = (imgName = 'Jay') => {
	const paths = `../../assets/music/singer/${imgName}.jpg`;
	const modules = import.meta.globEager(`../../assets/music/singer/*.jpg`);
	return modules[paths].default;
};

export const getCommonImg = (imgName) => {
	const paths = `../../assets/music/common/${imgName}.png`;
	const modules = import.meta.globEager(`../../assets/music/common/*.png`);
	return modules[paths].default;
};