/*
 * @Author: YinXuan
 * @Date: 2022-01-30 21:48:57
 * @LastEditTime: 2023-02-15 16:28:18
 * @Description: 
 */
// 引用静态图片
export const getPngSrc = name => {
	const path = `../assets/${name}.png`;
	const modules = import.meta.globEager("../assets/*.png")
	return modules[path].default;
};

export const getJpgSrc = name => {
	const path = `../assets/carousel/${name}.jpg`;
	const modules = import.meta.globEager("../assets/carousel/*.jpg")
	return modules[path].default;
};

export const getCardSrc = name => {
	const path = `../assets/card/${name}.png`;
	const modules = import.meta.globEager("../assets/card/*.png")
	return modules[path].default;
};

export const getHomeSrc = name => {
	const paths = `../assets/home/${name}.jpg`;
	const modules = import.meta.globEager(`../assets/home/*.jpg`);
	return modules[paths].default;
};