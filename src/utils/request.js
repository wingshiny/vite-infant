import axios from "axios";
import { message } from 'ant-design-vue';

// 创建axios实例
const service = axios.create({
	// 在请求地址前面加上baseURL
	baseURL: 'http://localhost:8080/',
	timeout: 6000,
});

// 请求拦截器
service.interceptors.request.use(config => {
	// 请求前做点什么？
	if (!config.data) {
		config.data = {};
	}

	// console.log(config)
	// 设置公共参数

	return config;
}, error => {
	// 处理请求错误

	return Promise.reject(error);
});



// 响应拦截器
service.interceptors.response.use(response => {
	// let res = respone.data; // 如果返回的结果是data.data的，嫌麻烦可以用这个，return res
	return response.data;
}, error => {
	const { response } = error;
	if (response) {
		switch (response.status) {
			case 400:
				message.error('请求无效');
				break;
			case 401:
				message.error("尚未登录，请重新登录");
				break;
			case 403:
				message.error("您没有权限这样做，请联系管理员");
				break;
			case 404:
				message.error("请求未找到");
				break;
			case 500:
				message.error("系统异常");
				break;
			case 504:
				message.error("请求超时，请稍后再试");
				break;
			default:
				message.error("系统异常");
				break;
		}
	}
	return Promise.reject(error);
});

export default service;