import { createRouter, createWebHashHistory } from "vue-router";
import Layout from "../layouts/index.vue";
import {introduceRoute} from "./others/introduce";
import {resumeRoute} from "./others/resume";

/**
 * Note: 子菜单仅当路由的children.length >= 1时才出现
 *
 * hidden: true                   设置为true时路由将显示在sidebar中(默认false)
 * alwaysShow: true               如果设置为true则总是显示在菜单根目录
 *                                如果不设置alwaysShow, 当路由有超过一个子路由时,
 *                                将会变为嵌套模式, 否则不会显示根菜单
 * redirect: noRedirect           如果设置noRedirect时，breadcrumb中点击将不会跳转
 * name:'router-name'             name用于<keep-alive> (必须设置!!!)
 * meta : {
    roles: ['admin','editor']    页面可访问角色设置
    title: 'title'               sidebar和breadcrumb显示的标题
    icon: 'svg-name'/'el-icon-x' sidebar中显示的图标
    breadcrumb: false            设置为false，将不会出现在面包屑中
    activeMenu: '/example/list'  如果设置一个path, sidebar将会在高亮匹配项
  }
 */
export const routes = [
	{
		path: "/",
		redirect: "/home",
		component: Layout,
		meta: { title: "导航"},
		children: [
			{
				path: "home",
				name: "Home",
				component: () => import("../views/Home.vue"),
				meta: { title: "首页"},
			},
			{
				path: "list",
				name: "List",
				component: () => import("../views/news/List.vue"),
				meta: { title: "首页"},
			},
		],
	},
	{
		path: "/article",
		name: "Article",
		component: () => import("../views/article/index.vue"),
		meta: { title: "文章"},
	},
	{
		path: "/article/detail",
		name: "Detail",
		component: () => import("../views/article/detail/detail.vue"),
		meta: { title: "博客"},
		children: [
			{
				path: "?id",
				name: "Detail",
				component: () => {
					import('../views/article/detail/detail.vue')
				},
			},
		],
	},
	{
		path: "/blog",
		name: "Blog",
		component: () => import("../views/blog/index.vue"),
		meta: { title: "Blog"},
	},
	{
		path: "/nba",
		name: "NBA",
		component: () => import("../views/nba/index.vue"),
		meta: { title: "NBA"},
	},
	{
		path: "/music",
		name: "Music",
		component: () => import("../views/music/index.vue"),
		meta: { title: "Music"},
	},
	{
		path: "/tools/",
		name: "Tools",
		component: () => import("../views/tools/index.vue"),
		children: [
			{
				path: "echarts/",
				name: "Echarts",
				component: () => import("../views/tools/echarts/index.vue"),
				children: [
					{
						path: "word/cloud",
						name: "Word Cloud",
						component: () => import("../views/tools/echarts/components/WordCloud.vue"),
					},
					{
						path: "cow",
						name: "cow",
						component: () => import("../views/tools/echarts/components/Cow.vue"),
					},
					{
						path: "clock",
						name: "clock",
						component: () => import("../views/tools/echarts/components/Clock.vue"),
					},
				],
			},
		],
	},
	{
		path: "/index",
		name: "Index",
		meta: { title: "首页"},
		component: () => import("../views/home/index.vue"),
	},
	introduceRoute,
	resumeRoute,
];

const router = createRouter({
	history: createWebHashHistory(),
	routes,
});

export default router;