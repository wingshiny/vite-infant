export const introduceRoute = {
    path: "/introduce",
    name: "Introduce",
    meta: { title: "首页"},
    component: () => import("../../views/introduce/index.vue"),
}