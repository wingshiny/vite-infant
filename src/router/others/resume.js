export const resumeRoute = {
    path: "/resume",
    name: "Resume",
    meta: { title: "个人简历"},
    component: () => import("../../views/resume/index.vue"),
}