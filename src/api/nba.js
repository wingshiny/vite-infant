import request from '../utils/request'

export function getNbaRank (data) {
	return request({
		url: '/juhe/fapig/nba/rank?key=' + data,
		method: 'get',
	})
}

// 产品列表
export function productsList () {
	return request({
		url: '/products/list',
		method: 'get'
	})
}

export const getNbaGame=function(e){
	return request(
		'post',
		'/251280/123',
		{qwe:e}
	)    //post方法以及传参    qwe是字段   e是参数
};