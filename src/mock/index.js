import user from './user';
import menu from './menu';
import oauth from './oauth';
import param from './param';

/**
 * 模拟数据mock
 *
 * mock是否开启模拟数据拦截
 * 如果开启会导致激素扫描报表导出下载失败
 * （开启则会将后端返回的数据自动转成json格式，但是Excel下载需要后端返回Blob文件格式，故隐藏）
 */

// const options = {mock: true};

// user(options);
//
// menu(options);
//
// oauth(options);
//
// param(options);
