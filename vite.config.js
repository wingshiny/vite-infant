import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import ViteComponents, { AntDesignVueResolver } from 'vite-plugin-components';
export default defineConfig({
	plugins: [
		vue(),
		/* ... */
		ViteComponents({
			customComponentResolvers: [AntDesignVueResolver()],
		}),
	],
	server: {
		host: '0.0.0.0',
		// 是否启用热加载，就是每次更新代码，是否需要重新刷新浏览器才能看到新代码效果
		hot: true,
		// 是否自动打开浏览器，默认为false
		open: true,
		port: 8080,
		proxy: {
			"/bili": {
				target: "http://api.bilibili.com",
				changeOrigin: true,
				rewrite: (path) => path.replace(/^\/bili/, '')
			},
			"/juhe": {
				target: "http://apis.juhe.cn",
				changeOrigin: true,
				rewrite: (path) => path.replace(/^\/juhe/, '')
			},
		}
	},
	resolve: {
		alias: {
			assets: "@/assets",
			components: "@/components",
			views: "@/views"
		},
	},
	cssPreprocessOptions: {
		less: {
			modifyVars: {
				'primary-color': '#FE5F23',
				'link-color': '#1890FFFF',
				'info-color': '#1890FFFF'
			},
			javascriptEnabled: true
		},
		scss: {}
	}
})
